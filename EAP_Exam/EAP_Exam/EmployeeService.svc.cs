﻿using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace EAP_Exam
{
    public class EmployeeService : IEmployeeService
    {
        EmployeeDataDataContext data = new EmployeeDataDataContext();

        public bool AddEmployee(EmployeeEXAM eml) {
            try {
                data.EmployeeEXAMs.InsertOnSubmit(eml);
                data.SubmitChanges();
                return true;
            } catch {
                return false;
            }
        }

        public List<EmployeeEXAM> SearchEmplyeeByDepartmentName(string name) {
            try {
                List<EmployeeEXAM> results = 
                    (from e in data.EmployeeEXAMs where SqlMethods.Like(e.departmentName, "%" + name + "%") select e).ToList(); 
                return results;
            } catch {
                return null;
            }
        }
    }
}
