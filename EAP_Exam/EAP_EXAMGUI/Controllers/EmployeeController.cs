﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EAP_EXAMGUI.Controllers
{
    public class EmployeeController : Controller
    {
        EmployeeReference.EmployeeServiceClient er = new EmployeeReference.EmployeeServiceClient();

        public ActionResult Search()
        {
            ViewBag.employee = null;
            return View();
        }

        [HttpPost]
        public ActionResult Search(Models.SearchModel searchData)
        {
            try
            {
                ViewBag.employee = er.SearchEmplyeeByDepartmentName(searchData.name);
                return View();
            }
            catch
            {
                ViewBag.employee = null;
                return View();
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(EmployeeReference.EmployeeEXAM employee)
        {
            try
            {
                var result =  er.AddEmployee(employee);
                if (result)
                {
                    return RedirectToAction("Search");
                }
                else
                {
                    return View();
                }
            }
            catch
            {
                return View();
            }
        }
    }
}
